
#include <Arduino.h>
#include <RotaryEncoder.h>
#include <M5Atom.h>

#define CLICK_ADDRESS 1
#define DIR_ADDRESS 2


// A pointer to the dynamic created rotary encoder instance.
// This will be done in setup()

#define PIN_IN1 22 //pin m5 rota 1 
#define PIN_IN2 21 //pin m5 rota 2
#define PIN_IN3 25 // pin click


RotaryEncoder encoder(PIN_IN1, PIN_IN2, RotaryEncoder::LatchMode::TWO03);
unsigned int debounce_btn_time =0;
#define DEBOUNCE_TIME 50;


//COULEURS_BOUTON
uint8_t DisBuff[2 + 5 * 5 * 3]; //couleur du bouton?
void setBuff(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 0] = Rdata;
    DisBuff[2 + i * 3 + 1] = Gdata;
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
}
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
    DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird?
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
  M5.dis.displaybuff(DisBuff);
}



void setup()
{
  // put your setup code here, to run once:
  M5.begin(true, false, true);
  delay(10);
  Wire.begin(26,32);

  setBuff(0x00, 0x99, 0x00);
  M5.dis.displaybuff(DisBuff);
  Serial.println("Serial Connection initialized");
  Serial.println("Setup_end");
  delay(1000);
  Serial.print("click ");
  Serial.println(0);
  Serial.print("dir ");
  Serial.println(0);
}

void loop()
{
  static int dir = 0;
  static bool dirChanged = 0;

  static int pos = 0;
  static bool posChanged = 0;

  static int button = 0;
  static bool buttonChanged = 0;

  //check_time();

  int newButton = !(analogRead(PIN_IN3)/4095); //Pin !digital read
  unsigned time_now = millis();
  if (button != newButton && time_now > debounce_btn_time )
  {
    button = newButton;
    buttonChanged = 1;
    debounce_btn_time = time_now + DEBOUNCE_TIME;
  }

  encoder.tick();


  int newPos = encoder.getPosition();
  if (pos != newPos ) 
  {
    if (pos>newPos)
      {
        dir=1;
      } else if (pos<newPos) {
        dir=-1;
      } else {
        dir=0;
      }
    pos = newPos;
    encoder.setPosition(pos);
    posChanged = 1;
  } // if


  if (posChanged)
  {

    Serial.print("dir ");
    Serial.println(dir);
    posChanged = 0;
    byte value = 0;
    //value = map(dir, -1, 1, 0, 127);
    value = random(0,127);
    Serial.print("dir_v ");
    Serial.println(value);
//    int error;
////    Wire.beginTransmission(DIR_ADDRESS);
////    error = Wire.endTransmission();
////    Serial.println(error); 
////    if(error==0 || error ==2)
////      {
//            Wire.beginTransmission(DIR_ADDRESS);
//            Wire.write(value);
//            Wire.endTransmission();  
////      }

           

    
  }
  if (buttonChanged)
    {
      buttonChanged = 0;
      Serial.print("click ");
      Serial.println(button);
      Wire.beginTransmission(CLICK_ADDRESS);
      if(button==1)
        {
          Wire.write(0x99);
          }else{
          Wire.write(0x00);
          }
      Wire.endTransmission();  
    } 

}

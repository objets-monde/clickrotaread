#define OSC_OUT_PORT 53001
#define OSC_IN_PORT 8888
//char * osc_prefix="/dm/ethbtn/btn";


#include "Wire.h"


#define I2C_DEV_ADDR 0x55

// ethPOE
#include <SPI.h>
#include <Ethernet2.h>
EthernetUDP udp;
const unsigned int osc_out_port = OSC_OUT_PORT;

#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19

//Changer les derniers "chiffres de la mac addresse"
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEE, 0xEF, 0xAE };
IPAddress ip_broadcast(255, 255, 255, 255);
// \ethPOE

// cnmat osc
#include <OSCMessage.h>
//


// m5 LED
#include "M5Atom.h"

// couleur du led
#define RED 0xff, 0x00, 0x00
#define YELLOW 0xff, 0xff, 0x00
#define GREEN 0x00, 0xff, 0x00
#define CYAN 0x00, 0xff, 0xff
#define BLUE 0x00, 0x00, 0xff
#define PINK 0xff, 0x00, 0xff
#define OFF 0x00, 0x00, 0x00

uint8_t DisBuff[2 + 5 * 5 * 3];
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
    DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird?
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
  M5.dis.displaybuff(DisBuff);
}

// \m5 LED
uint32_t i = 0;


void onReceive(int len){
  char buf[len];
  int i=0;
  //Serial.printf("onReceive[%d]: ", len);
  while(Wire.available()){
    buf[i]=Wire.read();
    i++;
  }
  String test= buf;
  test.remove(test.lastIndexOf(";")); //cut giberish with delimiter ;
  String key = getValue(test, ' ', 0); // get 1 element (key) 
  String valueString = getValue(test, ' ', 1);  // get 2 element
  int value=valueString.toInt();   // convert to int
  Serial.print(key);
  Serial.print(" ");
  Serial.print(value);
  Serial.println();
  send_osc(key, value);

}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void setup() {
  M5.begin(true, false, true);
   Serial.print("init ");
  set_m5_led(BLUE);
  //Serial.begin(115200);
  //Serial.setDebugOutput(true);
  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);
  // init DHCP
  Ethernet.begin(mac);
    udp.begin(OSC_IN_PORT);
  set_m5_led(PINK); // IF here; ethernet initialize
// Create broadcast IP
  ip_broadcast = Ethernet.localIP();
  ip_broadcast[3] = 255;

  // Print init state
  Serial.print("Button ip => ");
  Serial.print(Ethernet.localIP());
  Serial.print(" broadcasting => ");
  Serial.println(ip_broadcast);


  Wire.onReceive(onReceive);
  //Wire.onRequest(onRequest);
  Wire.begin((uint8_t)I2C_DEV_ADDR,26,32);

}

void loop() {

}

void send_osc(String prefix, int value)
{

  //Serial.println(OSC_PREFIX);
 //char * osc_prefix="/test";
  const char* osc_prefix = prefix.c_str();
  OSCMessage msg(osc_prefix);
  msg.add((int32_t)value); 
  //msg.add(((const char *)"time"));
  //msg.add(((int32_t)millis()));
  udp.beginPacket(ip_broadcast, osc_out_port);
  msg.send(udp); // send the bytes to the SLIP stream
  udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
}

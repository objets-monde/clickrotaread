#define OSC_OUT_PORT 50030
#define OSC_IN_PORT 8888

#include "Wire.h"


#define I2C_DEV_ADDR 0x55

// ethPOE
#include <SPI.h>
#include <Ethernet2.h>
EthernetUDP udp;
const unsigned int osc_out_port = OSC_OUT_PORT;

#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19

//Changer les derniers "chiffres de la mac addresse"
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEE, 0xEF, 0xAE };
IPAddress ip_broadcast(255, 255, 255, 255);

// \ethPOE
// micro OSC
#include <MicroOscUdp.h>
MicroOscUdp<1024> oscUdp(&udp, ip_broadcast, OSC_OUT_PORT);
// \micro osc

// m5 LED
#include "M5Atom.h"

// couleur du led
#define RED 0xff, 0x00, 0x00
#define YELLOW 0xff, 0xff, 0x00
#define GREEN 0x00, 0xff, 0x00
#define CYAN 0x00, 0xff, 0xff
#define BLUE 0x00, 0x00, 0xff
#define PINK 0xff, 0x00, 0xff
#define OFF 0x00, 0x00, 0x00

uint8_t DisBuff[2 + 5 * 5 * 3];
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
    DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird?
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
  M5.dis.displaybuff(DisBuff);
}

// \m5 LED
uint32_t i = 0;


void onReceive(int len){
  Serial.printf("onReceive[%d]: ", len);
  while(Wire.available()){
    Serial.write(Wire.read());
  }
  Serial.println();
}

void setup() {
  M5.begin(true, false, true);
   Serial.print("init ");
  set_m5_led(BLUE);
  //Serial.begin(115200);
  //Serial.setDebugOutput(true);
  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);
  // init DHCP
  Ethernet.begin(mac);
    udp.begin(OSC_IN_PORT);
  set_m5_led(PINK); // IF here; ethernet initialize
// Create broadcast IP
  ip_broadcast = Ethernet.localIP();
  ip_broadcast[3] = 255;

  // Print init state
  Serial.print("Button ip => ");
  Serial.print(Ethernet.localIP());
  Serial.print(" broadcasting => ");
  Serial.println(ip_broadcast);


  Wire.onReceive(onReceive);
  //Wire.onRequest(onRequest);
  Wire.begin((uint8_t)I2C_DEV_ADDR,26,32);

}

void loop() {

}

void send_osc_i2c()
{
  
  udp.beginPacket(ip_broadcast, osc_out_port);
  udp.endPacket(); // mark the end of the OSC Packet
}

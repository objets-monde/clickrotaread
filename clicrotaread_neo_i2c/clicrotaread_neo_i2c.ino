#include "Wire.h"
uint32_t i = 0;

#define I2C_DEV_ADDR 0x55

#include <Arduino.h>
#include <RotaryEncoder.h>
#include <M5Atom.h>


// A pointer to the dynamic created rotary encoder instance.
// This will be done in setup()

#define PIN_IN1 22 //pin m5
#define PIN_IN2 21 //pin m5
#define PIN_IN3 25


RotaryEncoder encoder(PIN_IN1, PIN_IN2, RotaryEncoder::LatchMode::TWO03);
unsigned int debounce_btn_time =0;
#define DEBOUNCE_TIME 50;


//COULEURS_BOUTON
uint8_t DisBuff[2 + 5 * 5 * 3]; //couleur du bouton?
void setBuff(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 0] = Rdata;
    DisBuff[2 + i * 3 + 1] = Gdata;
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
}
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
    DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird?
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
  M5.dis.displaybuff(DisBuff);
}



void setup()
{
  // put your setup code here, to run once:
  M5.begin(true, false, true);
  delay(10);
  setBuff(0x00, 0x99, 0x00);
  M5.dis.displaybuff(DisBuff);
  Wire.begin(26,32);
  delay(1000);
  Serial.println("Serial Connection initialized");

}

void loop()
{
  static int dir = 0;
  static bool dirChanged = 0;

  static int pos = 0;
  static bool posChanged = 0;

  static int button = 0;
  static bool buttonChanged = 0;

  //check_time();
  int newButton = !(analogRead(PIN_IN3)/4095); //Pin !digital read
  unsigned time_now = millis();
  if (button != newButton && time_now > debounce_btn_time )
  {
    button = newButton;
    debounce_btn_time = time_now + DEBOUNCE_TIME;
    send_key_value("/click", button);
  }
  
  encoder.tick();

  int newPos = encoder.getPosition();
  if (pos != newPos ) 
  {
    if (pos>newPos)
      {
        dir=1;
      } else if (pos<newPos) {
        dir=0;
      } else {
        dir=0;
      }    
    pos = newPos;
    encoder.setPosition(pos);
    send_key_value("/dir", dir);
    //send_key_value("/pos", pos);
    
  } // if
}


void send_key_value(String key, int value)
{
  Serial.printf("%s %u;\n", key, value);
  Wire.beginTransmission(I2C_DEV_ADDR);
  Wire.printf("%s %u;" , key, value);
  uint8_t error = Wire.endTransmission(true);
}

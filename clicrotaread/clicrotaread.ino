
#include <Arduino.h>
#include <RotaryEncoder.h>
#include <M5Atom.h>


// A pointer to the dynamic created rotary encoder instance.
// This will be done in setup()

#define PIN_IN1 22 //pin m5
#define PIN_IN2 21 //pin m5
#define PIN_IN3 25

unsigned long timeout = 0 ;
unsigned long lapse = 1000;
int max_pos = 540;
bool time_flag = 0;
bool bascule = 0;

RotaryEncoder encoder(PIN_IN1, PIN_IN2, RotaryEncoder::LatchMode::TWO03);



//COULEURS_BOUTON
uint8_t DisBuff[2 + 5 * 5 * 3]; //couleur du bouton?
void setBuff(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 0] = Rdata;
    DisBuff[2 + i * 3 + 1] = Gdata;
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
}
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
    DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird?
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
  M5.dis.displaybuff(DisBuff);
}



void setup()
{
  // put your setup code here, to run once:
  M5.begin(true, false, true);
  delay(10);
  setBuff(0x00, 0x99, 0x00);
  M5.dis.displaybuff(DisBuff);
  Serial.println("Serial Connection initialized");
  Serial.println("Setup_end");
  delay(1000);
  Serial.print("click ");
  Serial.println(0);
  Serial.print("pos ");
  Serial.println(0);
  Serial.print("dir ");
  Serial.println(0);
}

void loop()
{
  static int dir = 0;
  static bool dirChanged = 0;

  static int pos = 0;
  static bool posChanged = 0;

  static int button = 0;
  static bool buttonChanged = 0;

  check_time();

  int newButton = !digitalRead(PIN_IN3);
  if (button != newButton)
  {
    button = newButton;
    buttonChanged = 1;
    pos = 0;
    //encoder.setPosition(0);
  }

  encoder.tick();

//  int newDir = int(encoder.getDirection());
//  if (dir != newDir ) {
//    Serial.println("chek");
//    dir = newDir;
//    dirChanged = 1;
//  } // if


  int newPos = encoder.getPosition();
  if (pos != newPos ) 
  {
    if (pos>newPos)
      {
        dir=1;
      } else if (pos<newPos) {
        dir=-1;
      } else {
        dir=0;
      }
    
    //pos = constrain(newPos, -max_pos, max_pos);
    pos = newPos;
    encoder.setPosition(pos);
    posChanged = 1;
  } // if










  if (buttonChanged || posChanged  )
  {
    Serial.print("click ");
    Serial.println(button);
    Serial.print("pos ");
    Serial.println(pos);
    Serial.print("dir ");
    Serial.println(dir);
    //Serial.println(" ");
    posChanged = 0;
    buttonChanged = 0;
    dirChanged = 0;
    timeout = millis() + 100;

  }
}

void check_time()
{
  unsigned time_now = millis();
  int pos = encoder.getPosition();

  if (time_now > timeout && pos != 0)
  {
    int increment = 0;
    if (pos > 0)
    {
      increment = -1;
    } else {
      increment = 1;
    }
    //encoder.setPosition(pos + increment);

  }
}

# clickrotaread

![](_doc/clic_rota_read.drawio.svg)

[TOC]



## Behavior

Broadcast OSC on port 53001
* Button press 
* Direction


## Components
### M5_clickRotaRead 
![m5_clicrotaread](_doc/head.jpg)

#### Firmware
ClickRotaReadi2c

#### Connections
![](_doc/rotary_circuit.drawio.svg)

### MicroController
![](_doc/rotary_back.drawio.svg)

#### BOM
* 1 x [m5atom lite ](https://www.digikey.ca/en/products/detail/m5stack-technology-co-ltd/C008/12088545) 
* 1 x [m5atom mate ](https://www.digikey.ca/en/products/detail/m5stack-technology-co-ltd/A086/13148778)
* 1 x [Rotary Encoder ](https://www.amazon.ca/dp/B07T3672VK/)
* 1 x [DIN Rail Steel 4 holes section](https://www.digikey.ca/en/products/detail/altech-corporation/2511120-1M/8546913)
* 1 x [GROVE CABLE 4POS 50CM](https://www.digikey.ca/en/products/detail/m5stack-technology-co-ltd/A034-C/16370076?s=N4IgTCBcDaIIYAYDMAWAxiAugXyA) 
* 1 x 3D printed spacer 
* 2 x m2 rounded head machine screws 3 mm thread
* 2 x m2 standoff 5 mm
* 2 x m2 nut 
* LED white
*  [5/8 ball mount to 1/4 thread](https://www.amazon.ca/gp/product/B07571NL5G/)
  * Threaded ball with 1/4 20 tap 
  * 
* 1 x [1/4" 20tpi x ~30mm thread machine screw round head]
* 2 x [split lock washer 1/4" ]
* 2 x [1/4" 20tpi nut]
* 1 x [small rig articulated arm with clamp ](https://www.amazon.ca/dp/B00DJ5XH4O/)


### M5-ATOMPOE_I2C-OSC 
![](_doc/i2c_osc_top.jpg)

#### Connections

```mermaid
graph LR
Ethernet_POE -- RJ45 --> M5-POE_I2C-OSC -- I2C Cable--> m5_clickrotaRead 

```

#### BOM
* 1 x [m5atom poe](https://www.digikey.ca/en/products/detail/m5stack-technology-co-ltd/K052/13913922?s=N4IgTCBcDaILYFYCGAXA9nABABzQUxAF0BfIA)
* 1 x DIN Rail 8 holes 
  * M3 thread on the din rail 
  * 
* 2 x Small RIG mini Clamp
* 2 x 1/4 20tpi bolt 25mm thread(?)
* 2 x split ring washer
* 8 x Washer 
* 2 x flat head M3  25 mm (?)

### Mic Stand Chrome

1x [ Microphone stand, chrome, strait with round base 
* [Atlas Sound MS-12C](https://www.amazon.ca/dp/B0064REU40/)
* [K&M Stands 26000-500-02 ](https://www.amazon.ca/dp/B071JBZ47S/)





## mcu code




### Dependencies

#### ESP32 boards in arduino

##### in preference

```

https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

```

#### via Arduino libs

##### RotaryEncoder

By Matthias Hertel


### puredata

#### from source
https://gitlab.com/-/snippets/2330086


## Assembly images

[![](_doc/rotary_comp/IMG_5395.JPG)](_doc/rotary_brut/IMG_5395.JPG)

[![](_doc/rotary_comp/IMG_5405.JPG)](_doc/rotary_brut/IMG_5405.JPG)

[![](_doc/rotary_comp/IMG_5406.JPG)](_doc/rotary_brut/IMG_5406.JPG)

[![](_doc/rotary_comp/IMG_5407.JPG)](_doc/rotary_brut/IMG_5407.JPG)

[![](_doc/rotary_comp/IMG_5408.JPG)](_doc/rotary_brut/IMG_5408.JPG)

[![](_doc/rotary_comp/IMG_5411.JPG)](_doc/rotary_brut/IMG_5411.JPG)

[![](_doc/rotary_comp/IMG_5412.JPG)](_doc/rotary_brut/IMG_5412.JPG)

[![](_doc/rotary_comp/IMG_5414.JPG)](_doc/rotary_brut/IMG_5414.JPG)

[![](_doc/rotary_comp/IMG_5415.JPG)](_doc/rotary_brut/IMG_5415.JPG)

[![](_doc/rotary_comp/IMG_5416.JPG)](_doc/rotary_brut/IMG_5416.JPG)

[![](_doc/rotary_comp/IMG_5417.JPG)](_doc/rotary_brut/IMG_5417.JPG)